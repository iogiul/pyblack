Version 0.1.5 11/05/2023

- Added the option mandel21 (https://arxiv.org/pdf/2110.09254.pdf) as analytic approximation of the GW merging time.

Version 0.1.3 28/07/2022

- Constants and units are consistent with Astropy V. 5.1

Version 0.1.2, 17/06/2022

- Added adaptive euler as method in estimate_tgw

Version 0.1.1, 16/06/2022

- New methods based on analytic corrections added in the estimate_tgw to estimate the GW decay merging time
- The function estimate_tgw can now be called directly by the main module,
  e.g. from pyblack import estimate_tgw
